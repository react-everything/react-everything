import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./pages/App";
import { BrowserRouter, Route } from "react-router-dom";
import registerServiceWorker from "./registerServiceWorker";
import Header from "./components/header";
import Footer from "./components/footer";
import Scatterplot from "./pages/scatterplot";
import TinyMCE from "./pages/tinymce";

ReactDOM.render(
  <BrowserRouter>
    <div>
      <Route path="/" exact={true} component={App} />
      <Route
        path="/scatterplot"
        render={() => {
          return (
            <div>
              <Header />
              <Scatterplot />
              <Footer />
            </div>
          );
        }}
      />
      <Route
        path="/tinymce"
        render={() => {
          return (
            <div>
              <Header />
              <TinyMCE />
              <Footer />
            </div>
          );
        }}
      />
    </div>
  </BrowserRouter>,

  document.getElementById("root")
);

registerServiceWorker();
