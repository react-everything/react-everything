import React from "react";
import "../css/App.css";

var style = {
  backgroundColor: "#17A2B8",
  borderTop: "1px solid #E7E7E7",
  textAlign: "center",
  padding: "5px",
  position: "fixed",
  left: "0",
  bottom: "0",
  height: "30px",
  width: "100%",
};

function Footer() {
  return (
    <div>
      <div style={style}>
        <small>Copyright &copy; {new Date().getFullYear()} Ashane Edirisinghe | All Rights Reserved</small>
      </div>
    </div>
  );
}

export default Footer;
