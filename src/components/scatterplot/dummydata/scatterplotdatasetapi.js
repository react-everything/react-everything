export default {
  scatterdata: [
    {
      studentname: "StudentA",
      score: 90,
      time: 40,
    },
    {
      studentname: "StudentB",
      score: 70,
      time: 55,
    },
    {
      studentname: "StudentC",
      score: 70,
      time: 65,
    },
    {
      studentname: "StudentD",
      score: 60,
      time: 50,
    },
    {
      studentname: "StudentE",
      score: 20,
      time: 50,
    },
  ],
};
