export const convertSeries = (resultsFromApi) => {
  const refinedSeries = [];

  resultsFromApi.forEach((value) => {
    refinedSeries.push({
      name: value.studentname,
      symbolSize: 10,
      data: convertAxisIntoData(value.score,value.time),
      type: "scatter",
    });
    
  });
  return refinedSeries;
};

const convertAxisIntoData = (score,time) => {
  if (score < 70) {
    return [null, [score,time]];
  } else return [[score,time], null];
};
