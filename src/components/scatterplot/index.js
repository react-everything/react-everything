/**
 * https://echarts.apache.org/examples/en/editor.html?c=scatter-clustering
 * https://echarts.apache.org/examples/en/editor.html?c=scatter-nutrients
 * https://git.hust.cc/echarts-for-react/
 * https://www.npmjs.com/package/echarts-for-react
 * https://echarts.apache.org/en/option.html#xAxis.show
 */

import React, { useState, useEffect } from "react";
import ReactECharts from "echarts-for-react";
import scatterdataset from "./dummydata/scatterplotdatasetapi";
import { convertSeries } from "./ScatterUtils";

export default function Scatterplot() {
  const MAX_CHART_SIZE = { x_axis: 140, y_axis: 100 };
  const [testdata, setTestdata] = useState([
    {
      name: "X",
      type: "line",
      symbolSize: 0,
      data: [
        [0, 40],
        [MAX_CHART_SIZE.x_axis, 40],
      ],
      color: "#696969",
      silent: true,
      label: { show: false },
      lineStyle: {
        type: "dotted",
      },
    },
    {
      name: "Y",
      type: "line",
      symbolSize: 0,
      data: [
        [70, 0],
        [70, MAX_CHART_SIZE.y_axis],
      ],
      silent: true,
      label: { show: false },
      color: "#696969",
      lineStyle: {
        type: "dotted",
      },
    },
  ]);

  useEffect(() => {
    const fetchData = async () => {
      const scatterData = scatterdataset.scatterdata;
      // const prodData = await axios.get(
      //   "https://anly-agrtr-src-dev-lb-621065259.us-east-1.elb.amazonaws.com/api/v1/test",
      //   {
      //     headers: headers,
      //   }
      // );

      const scatterDataRefind = convertSeries(scatterData);
      setTestdata([...testdata, ...scatterDataRefind]);
    };
    fetchData();
    return () => {};
  }, []);

  var option = {
    // height:"200px",
    // width:"200px",
    title: {
      show: true,
      text: "Scatter Plot",
      left: 150,
    },
    xAxis: {
      max: MAX_CHART_SIZE.x_axis,
      splitNumber: MAX_CHART_SIZE.x_axis,
      name: "Maximum readiness score",
      nameLocation: "middle",
      // nameTextStyle: {
      //   fontSize: 20,
      // },
      nameGap: 35,
      type: "value",
      // axisLine: {
      //   show: true,
      // },
      splitLine: {
        show: false,
        lineStyle: {
          type: "dashed",
        },
      },
      axisTick: {
        show: false,
      },
      axisLabel: {
        formatter: function (value) {
          if (value === 70) {
            return value + "%";
          } else {
            return "";
          }
        },
      },
    },
    yAxis: {
      max: MAX_CHART_SIZE.y_axis,
      splitNumber: MAX_CHART_SIZE.y_axis,
      name: "Time",
      nameLocation: "middle",
      nameGap: 10,
      type: "value",
      nameRotate: 0,
      splitLine: {
        show: false,
        lineStyle: {
          type: "dashed",
        },
      },
      axisTick: {
        show: false,
      },
      axisLabel: {
        formatter: function (value) {
          if (value === 40) {
            return value + "min";
          } else {
            return "";
          }
        },
      },
    },

    tooltip: {
      trigger: "item", //change this
      formatter: "Student name : {a}<br />Score,Time : {c0}",
    },
    visualMap: {
      type: "piecewise",

      // min: 0,
      // max: 2,
      bottom: 0,
      left: 180,
      // splitNumber: 2,
      dimension: 2,
      pieces: [
        {
          value: 0,
          label: "Profecient",
          color: "#37A2DA",
        },
        {
          value: 1,
          label: "Need to learn",
          color: "#e06343",
        },
      ],
    },

    series: testdata,
  };

  return (
    <div>
      <ReactECharts option={option} />
    </div>
  );
}
