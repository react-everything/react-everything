import React, { Component } from "react";
import logo from "../logo.svg";
import "../css/App.css";

class App extends Component {
  state = {};

  render() {
    return (
      <div className="App">
        <header className="">
          <img src={logo} className="App-logo" alt="logo" />
        </header>
        <h1>React Everything</h1>
        <h2>SPIKES</h2>
        <hr />
        <div className="col-md-12 row">
          <a href="/scatterplot" className="btn btn-info mainpages">
            Scatter Plot
          </a>
          <a href="/tinymce" className="btn btn-info mainpages">
            Tiny MCE
          </a>
        </div>
      </div>
    );
  }
}

export default App;
