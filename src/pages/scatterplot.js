import React, { Component } from "react";
import Testscatter from "../components/scatterplot";

class ScatterPlot extends Component {
  componentDidMount() {
    console.log("consolelog " + process.env.REACT_APP_API_URL);
  }

  render() {
    return (
      <div className="loadscatterplot col-md-12">
        {/* <h4>Scatterplot component</h4> */}
        <br />
        <Testscatter />
      </div>
    );
  }
}

export default ScatterPlot;
